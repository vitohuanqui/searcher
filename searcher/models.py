from django.db import models

class New(models.Model):
	category = models.CharField(max_length=20);
	url = models.URLField();
	text = models.TextField();
	date = models.DateField();
	title = models.TextField();
	tags = models.TextField();
	product = models.FloatField();

class globalDictionary(models.Model):
	word = models.TextField();
	idNew = models.IntegerField();
	idftf = models.FloatField();
	date = models.DateField();

# Create your models here.
