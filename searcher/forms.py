from django import forms
from django.forms import ModelForm
from searcher.models import New

class NewForm(forms.Form):
    text = forms.CharField();
    date1 = forms.DateField();
    date2 = forms.DateField();