from django.shortcuts import render
from searcher.models import New
from searcher.models import globalDictionary
from searcher.forms import NewForm
from django.http import HttpResponse
import json
from datetime import datetime
from django.core.cache import cache 
from django.db.models import Count 
import nltk
from nltk.corpus import stopwords
from nltk.stem import *
from collections import OrderedDict

stemmer = SnowballStemmer('spanish')
stop = stopwords.words('spanish')
extras = ["'",",",".",";","(",")","[","]","{","}","\n"]
stop+=extras
result = {}

def deleteStopWords(new):
    output = []
    for w in nltk.word_tokenize(new):
        w = w.lower()
        if w not in stop:
            output.append(w)
    return output

def porter(new):
    output = []
    for w in new:
        output.append(stemmer.stem(w))
    return output

def modifyNews(news):
    for new in news:
        new.text = deleteStopWords(new.text)
        new.text = porter(new.text)
    return news

def globalMatrix(news):
    gd = {}
    print "global"
    N = len(news)
    news = modifyNews(news)
    print "limpio "
    for new in news:
        print new.id
        textFreqDist = nltk.FreqDist(new.text)        
        new.text = textFreqDist.most_common(len(new.text))
        if len(new.text)!=0:
            max = float(new.text[0][1])
            for w in new.text:
                #to_insert = globalDictionary(word = w[0], idNew = new.id, idftf = w[1]/max,date = new.date)
                #to_insert.save()
                if gd.get(w[0]):
                    tuplatmp = (w[1]/max,new.id,new.date)
                    gd[w[0]].append(tuplatmp)
                else:
                    gd[w[0]] = []
                    tuplatmp = (w[1]/max,new.id,new.date)
                    gd[w[0]].append(tuplatmp)
        del new
    #idfs = globalDictionary.objects.values('word').annotate(total=Count('word'))
    #for w in idfs:
    #    words = globalDictionary.objects.filter(word = w['word'])
    #    for j in words:
    #        j.idftf = j.idftf*N/w['total']
    #        n = New.objects.filter(id = j.idNew)
    #        n.product = n.product+(j.idftf*j.idftf)
    #        j.save()
    #        n.save()


        #with open('data.json') as data_file:
    #    globalDictionary = json.loads(data_file.read(),"utf-8")
    newQ = {}
    print "global TF"
    for k in gd.keys():
        idf = len(gd[k])
        for l in gd[k]:
            l = (l[0]*N/idf,l[1],l[2])
            if newQ.get(l[1]):
                newQ[l[1]]+= l[0]*l[0]
            else:
                newQ[l[1]] = l[0]*l[0]
    for k in gd.keys():
        for l in gd[k]:
            to_insert = globalDictionary(word = k, idNew = l[1], idftf = l[0]/max,date = l[2])
            to_insert.save()
    gd = {}
    for k in newQ.keys():
        n = New.objects.filter(id = k)
        n.product = newQ[k]
        n.save()
    newQ = {}
    print "global IDF"
#    for k in globalDictionary.keys():
 #       globalDictionary[k] = sorted(globalDictionary[k])
  #      for v in globalDictionary[k]:
   #         if newQ.get(v[1]):
    #            newQ[v[1]]+= v[0]*v[0]
     #       else:
      #          newQ[v[1]] = v[0]*v[0]

    #with open('data.json', 'w') as fp:
    #    json.dump(globalDictionary, fp)
    #with open('data1.json','w') as da:
    #    json.dump(newQ,da)
    #print "globaL X"

def idf(Nnews,kword,news):
    count = 0
    for new in news:
        if kword in new.text:
            count+=1
    return float(Nnews)/count



def startSparseMatrix(news,category,matrix):
    sparseMatrix= {}
    categoryMatrix = {}
    print "to modifyNews"
    news = modifyNews(news)
    i = 0
    print "cambio texto"
    for new in news:
        textFreqDist = nltk.FreqDist(new.text)
        new.text = textFreqDist.most_common(len(new.text))

    for new in news:
        max = float(new.text[0][1])
        for w in new.text:
            tmpTupla = (new.id,w[0],i)
            sparseMatrix[tmpTupla] = (w[1]/max)*(idf)
        i+=1
    print "creamos matriz sin idf itf"
    print len(sparseMatrix.keys())
    j = 0
    for k in sparseMatrix.keys():
        print len(sparseMatrix.keys()),"-",j
        j+=1 
        sparseMatrix[k] = (tf(news[k[2]].text,sparseMatrix[k])) * (idf(len(news),k[1],news))
        if categoryMatrix.get(('category',k[1],'category'))<sparseMatrix[k]:
            categoryMatrix[('category',k[1],'category')] = sparseMatrix[k]
    tupla = (categoryMatrix,sparseMatrix)
    return categoryMatrix

def firstStep(news,category):
    paso = len(news)/1000
    listDic = []
    i = 0
    j = paso
    while (j<len(news)):
        listDic.append(startSparseMatrix(news[i:j]))
        i+=paso
        if j+paso<len(news):
            j+=paso
        else:
            j=len(news)
    return listDic

def cos(query,matrix):
    keysToWork = []
    keys = matrix.keys()
    for k in keys:
        if k[0] == 'category':
            keysToWork.append(k)

    words = []
    for k in keysToWork:
        words.append(k[1])
    toWork = set(query,words)
    toWork = list(toWork)
    sum = sumQuery = sumCat = 0
    for k in toWork:
        sumQuery+= matrix[('category',k,'category')]**2
    for k in keysToWork:
        sumCat+= matrix[('category',k,'category')]**2
    for k in toWork:
        sum+= matrix[('category',k,'category')]*matrix[('category',k,'category')]
    return sum/((sumQuery**(0.5))*(sumCat**(0.5)))


def search(request,i):
    global result
    #if len(globalDictionary.keys())==0:
    #    news = New.objects.all()
    #    globalMatrix(news)
    #    print "fisnish"
    #print len(globalDictionary.keys())
    cosQuery = cosNew = 0
    if len(result.keys())==0:
        if request.method == 'POST':
            print "entrada"
            form = NewForm(request.POST)
            if form.is_valid():
                text = request.POST.get("text")
                date1 = request.POST.get("date1")
                date2 = request.POST.get("date2")
                print "valido"
                text = deleteStopWords(text)
                text = porter(text)
                print text
                for w in text:
                    word = globalDictionary.objects.filter(word = w,date__range=[date1, date2]).oder_by('idftf')
                    if len(words)!=0:
                        print '-----------------'
                        maxTFidf = word[-1].idftf
                        cosQuery+= maxTFidf*maxTFidf
                        for new in word:
                            if result.get(new.idNew):
                                print new.idNew
                                result[new.idNew] = result[new.idNew]+maxTFidf*new.idftf
                            else:
                                result[new.idNew] = maxTFidf*new.idftf
                print "paso hallar cosenos"
                cosNew = New.objects.filter(pk__in=globalResult.keys()).values('id','product')
                for k in cosNew:
                    cosNew = New.objects.filter(id = k)
                    result[k.id] = result[k.id]/(k.product**(0.5)+cosQuery**(0.5))
                
                result = OrderedDict(sorted(result.items(), key=lambda t: t[1],reverse=True))

                globalResult = result
                toSend = New.objects.filter(pk__in=globalResult.keys())[0+(20*int(i)):20+(20*int(i))]
                return render(request,
                                "search.html",
                                {"toSend":toSend,"results":range(int(i),int(i)+20)})
        else:   
            print "salida"
            form = NewForm()
        return render(request,
                      "index.html",
                      {'form': form})
    else:
        toSend = New.objects.filter(pk__in=result.keys())[0+(20*int(i)):20+(20*int(i))]
        return render(request,
                 "search.html",
                        {"toSend":toSend,"results":range(int(i),int(i)+20)})



    categorys = ['Turismo','Espectaculos','Internacional','tecnologia']
    for cat in categorys:
        print cat
        result = cache.get(cat)
        print cat 
        if not result:
            print 'termino query'
            print len(news)
            result = firstStep(news,cat)
            if cat == 'Turismo':
                Turismo = result
                cache.set(Turismo)
            elif cat == 'Espectaculos':
                Espectaculos = result
                cache.set(Espectaculos)
            elif cat == 'Internacional':
                Internacional = result
                cache.set(Internacional)
            elif cat == 'tecnologia':
                tecnologia = result
                cache.set(tecnologia)
    if request.method == 'POST':
        form = NewForm(request.POST)
        if form.is_valid():
            form.text = deleteStopWords(form.text)
            form.text = porter(form.text)
            print 'Turismo'
            cosTurismo = cos(form.text,Turismo)
            print 'Espectaculos'
            cosEspectaculos = cos(form.text,Espectaculos)
            print 'Internacional'
            cosInternacional = cos(form.text,Internacional)
            print 'tecnologia'
            cosTecnologia = cos(form.text,tecnologia)
            return HttpResponse(str(cosTurismo)+"-"+str(cosEspectaculos)+"-"+str(cosInternacional)+"-"+str(cosTecnologia))
    else:
        form = NewForm()
    return render(request,
                  "index.html",
                  {'form': form})

def setnews(request):
    print "Termino internacioal"
    news = New.objects.all()
    globalMatrix(news)
    return HttpResponse("Ready!")

def index(request):
    if request.method == 'POST':
        form = NewForm(request.POST)
        if form.is_valid():
            return HttpResponseRedirect('/admin/')
    else:
        form = NewForm()
    return render(request,
                  "index.html",
                  {'form': form})
# Create your views here.
