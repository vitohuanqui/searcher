# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('searcher', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='globalDictionary',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('word', models.CharField(max_length=20)),
                ('idNew', models.IntegerField()),
                ('idftf', models.FloatField()),
                ('date', models.DateField()),
            ],
        ),
        migrations.AddField(
            model_name='new',
            name='product',
            field=models.FloatField(default=0),
            preserve_default=False,
        ),
    ]
