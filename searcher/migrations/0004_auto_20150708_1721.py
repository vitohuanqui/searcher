# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('searcher', '0003_auto_20150708_0250'),
    ]

    operations = [
        migrations.AlterField(
            model_name='globaldictionary',
            name='word',
            field=models.TextField(),
        ),
    ]
