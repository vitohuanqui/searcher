from django.conf.urls import url

from searcher import views

urlpatterns = [
    url(r'setnews/$', views.setnews, name='setnews'),
    url(r'index/$', views.index, name='index'),
    url(r'^(?P<i>[0-20000]+)/$', views.search, name='search'),
]