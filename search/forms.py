from django import forms
from django.forms import ModelForm
from searcher.New import New

class SearchNewForm(forms.Form):
	class Meta:
	 	model = New
	 	fields = ['text','title','Sdate','Fdate']